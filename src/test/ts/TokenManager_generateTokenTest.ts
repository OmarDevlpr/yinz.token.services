// libs
import * as assert from "assert";
import { Chance } from "chance";
import Container from "@yinz/container/ts/Container";
import { TypeormDao, TypeormDataSource } from "@yinz/commons.data";
import { YinzConnection, YinzTransaction } from "@yinz/commons.data/ts/DataSource";
// import ArchToken from "@yinz/token.data/models/ArchToken";
import Token from "@yinz/token.data/models/Token";
import TokenManager from "main/ts/TokenManager";
import { Exception } from "@yinz/commons";



const container = new Container({
    paramsFile: 'params.yml',
    modules: [
        '@yinz/commons/ts',
        '@yinz/commons.data/ts',
        '@yinz/token.data/ts',        
        process.cwd() + '/dist/main/ts'
    ],
});

const chance = new Chance();
// const Exception = container.getClazz('Exception');
// const archTokenDao = container.getBean<TypeormDao<ArchToken>>('archTokenDao');
const tokenDao = container.getBean<TypeormDao<Token>>('tokenDao');
const dataSource = container.getBean<TypeormDataSource>('typeormDataSource');

const tokenManager = container.getBean<TokenManager>("tokenManager");

let conn: YinzConnection;
let trans: YinzTransaction;



describe('| access.data.TokenManager <generateToken>', function () {

    before(() => {
        return new Promise(async (resolve) => {
            conn = await dataSource.getConn();
            resolve(conn);
        })
    });

    after(() => {
        return new Promise(async (resolve) => {
            await dataSource.relConn(conn)
            resolve(true);
        })
    });

    beforeEach(() => {
        return new Promise(async (resolve) => {
            trans = await dataSource.startTrans(conn);
            resolve(trans);
        })
    });

    afterEach(() => {
        return new Promise(async (resolve) => {
            await dataSource.rollbackTrans(trans)
            resolve(true);
        })
    });

    it( '\n' +
        '       | Conditions: \n' +
        '       | - owner is valid \n' +
        '       | - data not supplied : \n' +
        '       | - options not supplied: \n' +
        '       | Expected: \n' +
        '       | -> should generate token using default parameters.' +
    '', async () => {

        // 1. prepare test-data
        let owner = chance.string({ length: 10 });
        let token = new Token();
        let options = {user: "__super_user__"};

        // 2. execute test        
        token = await tokenManager.generateToken(trans, owner, null, options)

        // 3. check result 
        // 3.1 check generated token

        assert.ok(token);
        assert.ok(token.code);
        assert.ok(token.code.match(/^[A-Za-z]{8}$/));

        assert.strictEqual(token.owner, owner);
        assert.strictEqual(token.validityDuration, 900000);
        assert.strictEqual(token.maxNumberOfUse, 1);
        assert.strictEqual(token.numberOfUse, 0);

        // 3.2 check persisted token

        let findToken = await tokenDao.findById(trans, token.id, options);

        assert.ok(findToken);
        assert.ok(findToken.code);
        assert.ok(findToken.code.match(/^[A-Za-z]{8}$/));

        assert.strictEqual(findToken.owner, owner);
        assert.strictEqual(findToken.validityDuration, 900000);
        assert.strictEqual(findToken.maxNumberOfUse, 1);
        assert.strictEqual(findToken.numberOfUse, 0);
        

    });


    it('\n' +
        '       | Conditions: \n' +
        '       | - owner is valid \n' +
        '       | - data is object : \n' +
        '       | - options not supplied: \n' +
        '       | Expected: \n' +
        '       | -> should generate token using default parameters. \n' +
        '       | -> should return the token data. \n' +
        '', async () => {

            // 1. prepare test-data
            let owner = chance.string({ length: 10 });
            let token = new Token();
            let options = { user: "__super_user__" };
            let tokenData = {
                field1: chance.string({ length: 10 }),
                field2: chance.string({ length: 10 }),
                field3: chance.string({ length: 10 }),
            }

            // 2. execute test        
            token = await tokenManager.generateToken(trans, owner, tokenData, options)

            // 3. check result 
            // 3.1 check generated token

            assert.ok(token);
            assert.ok(token.code);
            assert.ok(token.code.match(/^[A-Za-z]{8}$/));

            assert.strictEqual(token.owner, owner);
            assert.strictEqual(token.validityDuration, 900000);
            assert.strictEqual(token.maxNumberOfUse, 1);
            assert.strictEqual(token.numberOfUse, 0);

            assert.deepStrictEqual(token.data, tokenData);

            // 3.2 check persisted token

            let findToken = await tokenDao.findById(trans, token.id, options);

            assert.ok(findToken);
            assert.ok(findToken.code);
            assert.ok(findToken.code.match(/^[A-Za-z]{8}$/));

            assert.strictEqual(findToken.owner, owner);
            assert.strictEqual(findToken.validityDuration, 900000);
            assert.strictEqual(findToken.maxNumberOfUse, 1);
            assert.strictEqual(findToken.numberOfUse, 0);

            assert.deepStrictEqual(JSON.parse(findToken.data), tokenData);


        });


    it('\n' +
        '       | Conditions: \n' +
        '       | - owner is valid \n' +
        '       | - data is object : \n' +
        '       | - options length is 10 : \n' +
        '       | - rest of options not supplied: \n' +
        '       | Expected: \n' +
        '       | -> should generate token using the supplied length parameters.\n' +
        '       | -> should return the token data.\n' +
        '', async () => {

            // 1. prepare test-data
            let owner = chance.string({ length: 10 });
            let token = new Token();
            let options = { user: "__super_user__" };
            let tokenData = {
                field1: chance.string({ length: 10 }),
                field2: chance.string({ length: 10 }),
                field3: chance.string({ length: 10 }),
            }

            // 2. execute test        
            token = await tokenManager.generateToken(trans, owner, tokenData, {...options, token: {length: 10} })

            // 3. check result 
            // 3.1 check generated token

            assert.ok(token);
            assert.ok(token.code);
            assert.ok(token.code.match(/^[A-Za-z]{10}$/));

            assert.strictEqual(token.owner, owner);
            assert.strictEqual(token.validityDuration, 900000);
            assert.strictEqual(token.maxNumberOfUse, 1);
            assert.strictEqual(token.numberOfUse, 0);

            assert.deepStrictEqual(token.data, tokenData);

            // 3.2 check persisted token

            let findToken = await tokenDao.findById(trans, token.id, options);

            assert.ok(findToken);
            assert.ok(findToken.code);
            assert.ok(findToken.code.match(/^[A-Za-z]{10}$/));

            assert.strictEqual(findToken.owner, owner);
            assert.strictEqual(findToken.validityDuration, 900000);
            assert.strictEqual(findToken.maxNumberOfUse, 1);
            assert.strictEqual(findToken.numberOfUse, 0);

            assert.deepStrictEqual(JSON.parse(findToken.data), tokenData);


        });



    it('\n' +
        '       | Conditions: \n' +
        '       | - owner is valid \n' +
        '       | - data is object : \n' +
        '       | - options length is 10 : \n' +
        '       | - options type is set to `A` : \n' +
        '       | - rest of options not supplied: \n' +
        '       | Expected: \n' +
        '       | -> should generate token using the supplied length parameters.\n' +
        '       | -> should generate token with aplha code explicitly\n' +
        '       | -> should return the token data.\n' +
        '', async () => {

            // 1. prepare test-data
            let owner = chance.string({ length: 10 });
            let token = new Token();
            let options = { user: "__super_user__" };
            let tokenData = {
                field1: chance.string({ length: 10 }),
                field2: chance.string({ length: 10 }),
                field3: chance.string({ length: 10 }),
            }

            // 2. execute test        
            token = await tokenManager.generateToken(trans, owner, tokenData, { ...options, token: { length: 10, type : "A" } })

            // 3. check result 
            // 3.1 check generated token

            assert.ok(token);
            assert.ok(token.code);
            assert.ok(token.code.match(/^[A-Za-z]{10}$/));

            assert.strictEqual(token.owner, owner);
            assert.strictEqual(token.validityDuration, 900000);
            assert.strictEqual(token.maxNumberOfUse, 1);
            assert.strictEqual(token.numberOfUse, 0);

            assert.deepStrictEqual(token.data, tokenData);

            // 3.2 check persisted token

            let findToken = await tokenDao.findById(trans, token.id, options);

            assert.ok(findToken);
            assert.ok(findToken.code);
            assert.ok(findToken.code.match(/^[A-Za-z]{10}$/));

            assert.strictEqual(findToken.owner, owner);
            assert.strictEqual(findToken.validityDuration, 900000);
            assert.strictEqual(findToken.maxNumberOfUse, 1);
            assert.strictEqual(findToken.numberOfUse, 0);

            assert.deepStrictEqual(JSON.parse(findToken.data), tokenData);


        });


    it('\n' +
        '       | Conditions: \n' +
        '       | - owner is valid \n' +
        '       | - data is object : \n' +
        '       | - options length is 10 : \n' +
        '       | - options type is set to `N` : \n' +
        '       | - rest of options not supplied: \n' +
        '       | Expected: \n' +
        '       | -> should generate token using the supplied length parameters.\n' +
        '       | -> should generate token with numeric code explicitly\n' +
        '       | -> should return the token data.\n' +
        '', async () => {

            // 1. prepare test-data
            let owner = chance.string({ length: 10 });
            let token = new Token();
            let options = { user: "__super_user__" };
            let tokenData = {
                field1: chance.string({ length: 10 }),
                field2: chance.string({ length: 10 }),
                field3: chance.string({ length: 10 }),
            }

            // 2. execute test        
            token = await tokenManager.generateToken(trans, owner, tokenData, { ...options, token: { length: 10, type: "N" } })

            // 3. check result 
            // 3.1 check generated token

            assert.ok(token);
            assert.ok(token.code);
            assert.ok(token.code.match(/^[0-9]{10}$/));

            assert.strictEqual(token.owner, owner);
            assert.strictEqual(token.validityDuration, 900000);
            assert.strictEqual(token.maxNumberOfUse, 1);
            assert.strictEqual(token.numberOfUse, 0);

            assert.deepStrictEqual(token.data, tokenData);

            // 3.2 check persisted token

            let findToken = await tokenDao.findById(trans, token.id, options);

            assert.ok(findToken);
            assert.ok(findToken.code);
            assert.ok(findToken.code.match(/^[0-9]{10}$/));

            assert.strictEqual(findToken.owner, owner);
            assert.strictEqual(findToken.validityDuration, 900000);
            assert.strictEqual(findToken.maxNumberOfUse, 1);
            assert.strictEqual(findToken.numberOfUse, 0);

            assert.deepStrictEqual(JSON.parse(findToken.data), tokenData);


        });


    it('\n' +
        '       | Conditions: \n' +
        '       | - owner is valid \n' +
        '       | - data is object : \n' +
        '       | - options length is 10 : \n' +
        '       | - options type is set to `AN` : \n' +
        '       | - rest of options not supplied: \n' +
        '       | Expected: \n' +
        '       | -> should generate token using the supplied length parameters.\n' +
        '       | -> should generate token with alpha numeric code\n' +
        '       | -> should return the token data.\n' +
        '', async () => {

            // 1. prepare test-data
            let owner = chance.string({ length: 10 });
            let token = new Token();
            let options = { user: "__super_user__" };
            let tokenData = {
                field1: chance.string({ length: 10 }),
                field2: chance.string({ length: 10 }),
                field3: chance.string({ length: 10 }),
            }

            // 2. execute test        
            token = await tokenManager.generateToken(trans, owner, tokenData, { ...options, token: { length: 10, type: "AN" } })

            // 3. check result 
            // 3.1 check generated token

            assert.ok(token);
            assert.ok(token.code);
            assert.ok(token.code.match(/^[0-9A-Za-z]{10}$/));

            assert.strictEqual(token.owner, owner);
            assert.strictEqual(token.validityDuration, 900000);
            assert.strictEqual(token.maxNumberOfUse, 1);
            assert.strictEqual(token.numberOfUse, 0);

            assert.deepStrictEqual(token.data, tokenData);

            // 3.2 check persisted token

            let findToken = await tokenDao.findById(trans, token.id, options);

            assert.ok(findToken);
            assert.ok(findToken.code);
            assert.ok(findToken.code.match(/^[0-9A-Za-z]{10}$/));

            assert.strictEqual(findToken.owner, owner);
            assert.strictEqual(findToken.validityDuration, 900000);
            assert.strictEqual(findToken.maxNumberOfUse, 1);
            assert.strictEqual(findToken.numberOfUse, 0);

            assert.deepStrictEqual(JSON.parse(findToken.data), tokenData);


        });


    it('\n' +
        '       | Conditions: \n' +
        '       | - owner is valid \n' +
        '       | - data is object : \n' +
        '       | - options length is 10 : \n' +
        '       | - options type is set to `S` : \n' +
        '       | - rest of options not supplied: \n' +
        '       | Expected: \n' +
        '       | -> should generate token using the supplied length parameters.\n' +
        '       | -> should generate token with string code\n' +
        '       | -> should return the token data.\n' +
        '', async () => {

            // 1. prepare test-data
            let owner = chance.string({ length: 10 });
            let token = new Token();
            let options = { user: "__super_user__" };
            let tokenData = {
                field1: chance.string({ length: 10 }),
                field2: chance.string({ length: 10 }),
                field3: chance.string({ length: 10 }),
            }

            // 2. execute test        
            token = await tokenManager.generateToken(trans, owner, tokenData, { ...options, token: { length: 10, type: "S" } })

            // 3. check result 
            // 3.1 check generated token

            assert.ok(token);
            assert.ok(token.code);
            assert.ok(token.code.match(/^[ -~]{10}$/));

            assert.strictEqual(token.owner, owner);
            assert.strictEqual(token.validityDuration, 900000);
            assert.strictEqual(token.maxNumberOfUse, 1);
            assert.strictEqual(token.numberOfUse, 0);

            assert.deepStrictEqual(token.data, tokenData);

            // 3.2 check persisted token

            let findToken = await tokenDao.findById(trans, token.id, options);

            assert.ok(findToken);
            assert.ok(findToken.code);
            assert.ok(findToken.code.match(/^[ -~]{10}$/));

            assert.strictEqual(findToken.owner, owner);
            assert.strictEqual(findToken.validityDuration, 900000);
            assert.strictEqual(findToken.maxNumberOfUse, 1);
            assert.strictEqual(findToken.numberOfUse, 0);

            assert.deepStrictEqual(JSON.parse(findToken.data), tokenData);


        });



    it('\n' +
        '       | Conditions: \n' +
        '       | - owner is valid \n' +
        '       | - data is object : \n' +
        '       | - options length is 10 : \n' +
        '       | - options type is set to `S` : \n' +
        '       | - options modifier is set to `N` : \n' +
        '       | - rest of options not supplied: \n' +
        '       | Expected: \n' +
        '       | -> should generate token using the supplied length parameters.\n' +
        '       | -> should generate token with string code\n' +
        '       | -> should return the token data.\n' +
        '', async () => {

            // 1. prepare test-data
            let owner = chance.string({ length: 10 });
            let token = new Token();
            let options = { user: "__super_user__" };
            let tokenData = {
                field1: chance.string({ length: 10 }),
                field2: chance.string({ length: 10 }),
                field3: chance.string({ length: 10 }),
            }

            // 2. execute test        
            token = await tokenManager.generateToken(trans, owner, tokenData, { ...options, token: { length: 10, type: "S", modifier: "N" } })

            // 3. check result 
            // 3.1 check generated token

            assert.ok(token);
            assert.ok(token.code);
            assert.ok(token.code.match(/^[ -~]{10}$/));

            assert.strictEqual(token.owner, owner);
            assert.strictEqual(token.validityDuration, 900000);
            assert.strictEqual(token.maxNumberOfUse, 1);
            assert.strictEqual(token.numberOfUse, 0);

            assert.deepStrictEqual(token.data, tokenData);

            // 3.2 check persisted token

            let findToken = await tokenDao.findById(trans, token.id, options);

            assert.ok(findToken);
            assert.ok(findToken.code);
            assert.ok(findToken.code.match(/^[ -~]{10}$/));

            assert.strictEqual(findToken.owner, owner);
            assert.strictEqual(findToken.validityDuration, 900000);
            assert.strictEqual(findToken.maxNumberOfUse, 1);
            assert.strictEqual(findToken.numberOfUse, 0);

            assert.deepStrictEqual(JSON.parse(findToken.data), tokenData);


        });


    it('\n' +
        '       | Conditions: \n' +
        '       | - owner is valid \n' +
        '       | - data is object : \n' +
        '       | - options length is 10 : \n' +
        '       | - options type is set to `A` : \n' +
        '       | - options modifier is set to `U` : \n' +
        '       | - rest of options not supplied: \n' +
        '       | Expected: \n' +
        '       | -> should generate token using the supplied length parameters.\n' +
        '       | -> should generate token with aplha code\n' +
        '       | -> should generate token that is uppercase\n' +
        '       | -> should return the token data.\n' +
        '', async () => {

            // 1. prepare test-data
            let owner = chance.string({ length: 10 });
            let token = new Token();
            let options = { user: "__super_user__" };
            let tokenData = {
                field1: chance.string({ length: 10 }),
                field2: chance.string({ length: 10 }),
                field3: chance.string({ length: 10 }),
            }

            // 2. execute test        
            token = await tokenManager.generateToken(trans, owner, tokenData, { ...options, token: { length: 10, type: "A", modifier: "U" } })

            // 3. check result 
            // 3.1 check generated token

            assert.ok(token);
            assert.ok(token.code);
            assert.ok(token.code.match(/^[A-Z]{10}$/));

            assert.strictEqual(token.owner, owner);
            assert.strictEqual(token.validityDuration, 900000);
            assert.strictEqual(token.maxNumberOfUse, 1);
            assert.strictEqual(token.numberOfUse, 0);

            assert.deepStrictEqual(token.data, tokenData);

            // 3.2 check persisted token

            let findToken = await tokenDao.findById(trans, token.id, options);

            assert.ok(findToken);
            assert.ok(findToken.code);
            assert.ok(findToken.code.match(/^[A-Z]{10}$/));

            assert.strictEqual(findToken.owner, owner);
            assert.strictEqual(findToken.validityDuration, 900000);
            assert.strictEqual(findToken.maxNumberOfUse, 1);
            assert.strictEqual(findToken.numberOfUse, 0);

            assert.deepStrictEqual(JSON.parse(findToken.data), tokenData);


        });



    it('\n' +
        '       | Conditions: \n' +
        '       | - owner is valid \n' +
        '       | - data is object : \n' +
        '       | - options length is 10 : \n' +
        '       | - options type is set to `A` : \n' +
        '       | - options modifier is set to `L` : \n' +
        '       | - rest of options not supplied: \n' +
        '       | Expected: \n' +
        '       | -> should generate token using the supplied length parameters.\n' +
        '       | -> should generate token with aplha code\n' +
        '       | -> should generate token that is lowercase\n' +
        '       | -> should return the token data.\n' +
        '', async () => {

            // 1. prepare test-data
            let owner = chance.string({ length: 10 });
            let token = new Token();
            let options = { user: "__super_user__" };
            let tokenData = {
                field1: chance.string({ length: 10 }),
                field2: chance.string({ length: 10 }),
                field3: chance.string({ length: 10 }),
            }

            // 2. execute test        
            token = await tokenManager.generateToken(trans, owner, tokenData, { ...options, token: { length: 10, type: "A", modifier: "U" } })

            // 3. check result 
            // 3.1 check generated token

            assert.ok(token);
            assert.ok(token.code);
            assert.ok(token.code.match(/^[A-Z]{10}$/));

            assert.strictEqual(token.owner, owner);
            assert.strictEqual(token.validityDuration, 900000);
            assert.strictEqual(token.maxNumberOfUse, 1);
            assert.strictEqual(token.numberOfUse, 0);

            assert.deepStrictEqual(token.data, tokenData);

            // 3.2 check persisted token

            let findToken = await tokenDao.findById(trans, token.id, options);

            assert.ok(findToken);
            assert.ok(findToken.code);
            assert.ok(findToken.code.match(/^[A-Z]{10}$/));

            assert.strictEqual(findToken.owner, owner);
            assert.strictEqual(findToken.validityDuration, 900000);
            assert.strictEqual(findToken.maxNumberOfUse, 1);
            assert.strictEqual(findToken.numberOfUse, 0);

            assert.deepStrictEqual(JSON.parse(findToken.data), tokenData);


        });



    it('\n' +
        '       | Conditions: \n' +
        '       | - owner is valid \n' +
        '       | - data is object : \n' +
        '       | - options length is 10 : \n' +
        '       | - options type is set to `A` : \n' +
        '       | - options modifier is set to `L` : \n' +
        '       | - options validityDuration is set 1000 : \n' +
        '       | - rest of options not supplied: \n' +
        '       | Expected: \n' +
        '       | -> should generate token using the supplied length parameters.\n' +
        '       | -> should generate token with aplha code\n' +
        '       | -> should generate token that is lowercase\n' +
        '       | -> should generate token that is is valid for 1000\n' +
        '       | -> should return the token data.\n' +
        '', async () => {

            // 1. prepare test-data
            let owner = chance.string({ length: 10 });
            let token = new Token();
            let options = { user: "__super_user__" };
            let tokenData = {
                field1: chance.string({ length: 10 }),
                field2: chance.string({ length: 10 }),
                field3: chance.string({ length: 10 }),
            }

            // 2. execute test        
            token = await tokenManager.generateToken(trans, owner, tokenData, { ...options, token: { length: 10, type: "A", modifier: "U", validityDuration: 1000 } })

            // 3. check result 
            // 3.1 check generated token

            assert.ok(token);
            assert.ok(token.code);
            assert.ok(token.code.match(/^[A-Z]{10}$/));

            assert.strictEqual(token.owner, owner);
            assert.strictEqual(token.validityDuration, 1000);
            assert.strictEqual(token.maxNumberOfUse, 1);
            assert.strictEqual(token.numberOfUse, 0);

            assert.deepStrictEqual(token.data, tokenData);

            // 3.2 check persisted token

            let findToken = await tokenDao.findById(trans, token.id, options);

            assert.ok(findToken);
            assert.ok(findToken.code);
            assert.ok(findToken.code.match(/^[A-Z]{10}$/));

            assert.strictEqual(findToken.owner, owner);
            assert.strictEqual(findToken.validityDuration, 1000);
            assert.strictEqual(findToken.maxNumberOfUse, 1);
            assert.strictEqual(findToken.numberOfUse, 0);

            assert.deepStrictEqual(JSON.parse(findToken.data), tokenData);


        });



    it('\n' +
        '       | Conditions: \n' +
        '       | - owner is valid \n' +
        '       | - data is object : \n' +
        '       | - options length is 10 : \n' +
        '       | - options type is set to `A` : \n' +
        '       | - options modifier is set to `L` : \n' +
        '       | - options validityDuration is set 1000 : \n' +
        '       | - options maxNumberOfUse is set 10 : \n' +
        '       | - rest of options not supplied: \n' +
        '       | Expected: \n' +
        '       | -> should generate token using the supplied length parameters.\n' +
        '       | -> should generate token with aplha code\n' +
        '       | -> should generate token that is lowercase\n' +
        '       | -> should generate token that is is valid for 1000\n' +
        '       | -> should generate token that is usable 10 times\n' +
        '       | -> should return the token data.\n' +
        '', async () => {

            // 1. prepare test-data
            let owner = chance.string({ length: 10 });
            let token = new Token();
            let options = { user: "__super_user__" };
            let tokenData = {
                field1: chance.string({ length: 10 }),
                field2: chance.string({ length: 10 }),
                field3: chance.string({ length: 10 }),
            }

            // 2. execute test        
            token = await tokenManager.generateToken(trans, owner, tokenData, { ...options, token: {
                length: 10, type: "A", modifier: "U", validityDuration: 1000, maxNumberOfUse: 10 
            }})

            // 3. check result 
            // 3.1 check generated token

            assert.ok(token);
            assert.ok(token.code);
            assert.ok(token.code.match(/^[A-Z]{10}$/));

            assert.strictEqual(token.owner, owner);
            assert.strictEqual(token.validityDuration, 1000);
            assert.strictEqual(token.maxNumberOfUse, 10);
            assert.strictEqual(token.numberOfUse, 0);

            assert.deepStrictEqual(token.data, tokenData);

            // 3.2 check persisted token

            let findToken = await tokenDao.findById(trans, token.id, options);

            assert.ok(findToken);
            assert.ok(findToken.code);
            assert.ok(findToken.code.match(/^[A-Z]{10}$/));

            assert.strictEqual(findToken.owner, owner);
            assert.strictEqual(findToken.validityDuration, 1000);
            assert.strictEqual(findToken.maxNumberOfUse, 10);
            assert.strictEqual(findToken.numberOfUse, 0);

            assert.deepStrictEqual(JSON.parse(findToken.data), tokenData);


        });


    it('\n' +
        '       | Conditions: \n' +
        '       | - owner is undefined \n' +
        '       | - data not supplied : \n' +
        '       | - options not supplied: \n' +
        '       | Expected: \n' +
        '       | -> should reject the request with ERR__TOKEN_SERVICES__TOKEN_MANAGER__INV_OWNER.' +
        '', async () => {

            // 1. prepare test-data
            let owner = undefined as any;            
            let options = { user: "__super_user__" };

            // 2. execute test        

            try {
                await tokenManager.generateToken(trans, owner, null, options)
            }
            catch (e) {

                // 3. check result 

                assert.ok(e instanceof Exception);
                assert.strictEqual(e.reason, "ERR__TOKEN_SERVICES__TOKEN_MANAGER__INV_OWNER")

            }                                                

        });

    it('\n' +
        '       | Conditions: \n' +
        '       | - owner is valid \n' +
        '       | - data not supplied  \n' +
        '       | - options length is negative \n' +
        '       | Expected: \n' +
        '       | -> should reject the request with ERR__TOKEN_SERVICES__TOKEN_MANAGER__INV_LENGTH.' +
        '', async () => {

            // 1. prepare test-data
            let owner = undefined as any;            
            let options = { user: "__super_user__" };

            // 2. execute test        

            try {
                await tokenManager.generateToken(trans, owner, null, {...options, token : {length: -1}} );
            }
            catch (e) {

                // 3. check result 

                assert.ok(e instanceof Exception);
                assert.strictEqual(e.reason, "ERR__TOKEN_SERVICES__TOKEN_MANAGER__INV_LENGTH")

            }

        });

    it('\n' +
        '       | Conditions: \n' +
        '       | - owner is valid \n' +
        '       | - data not supplied  \n' +
        '       | - options validityDuration is negative \n' +
        '       | Expected: \n' +
        '       | -> should reject the request with ERR__TOKEN_SERVICES__TOKEN_MANAGER__INV_VALIDITY_DURATION.' +
        '', async () => {

            // 1. prepare test-data
            let owner = undefined as any;
            let options = { user: "__super_user__" };

            // 2. execute test        

            try {
                await tokenManager.generateToken(trans, owner, null, { ...options, token: { validityDuration: -1 } });
            }
            catch (e) {

                // 3. check result 

                assert.ok(e instanceof Exception);
                assert.strictEqual(e.reason, "ERR__TOKEN_SERVICES__TOKEN_MANAGER__INV_VALIDITY_DURATION")

            }

        });


    it('\n' +
        '       | Conditions: \n' +
        '       | - owner is valid \n' +
        '       | - data not supplied  \n' +
        '       | - options validityDuration is negative \n' +
        '       | Expected: \n' +
        '       | -> should reject the request with ERR__TOKEN_SERVICES__TOKEN_MANAGER__INV_MAX_NUM_OF_USE.' +
        '', async () => {

            // 1. prepare test-data
            let owner = undefined as any;
            let options = { user: "__super_user__" };

            // 2. execute test        

            try {
                await tokenManager.generateToken(trans, owner, null, { ...options, token: { maxNumberOfUse: -1 } });
            }
            catch (e) {

                // 3. check result 

                assert.ok(e instanceof Exception);
                assert.strictEqual(e.reason, "ERR__TOKEN_SERVICES__TOKEN_MANAGER__INV_MAX_NUM_OF_USE")

            }

        });
        
});

