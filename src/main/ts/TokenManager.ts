import { Logger, Exception, YinzOptions, Asserts } from "@yinz/commons";
import Token  from "@yinz/token.data/models/Token";
import ArchToken  from "@yinz/token.data/models/ArchToken";
import { YinzTransaction, YinzConnection } from "@yinz/commons.data/ts/DataSource";
import * as crypto from "crypto";
import {TypeormDao} from "@yinz/commons.data";
import * as moment from "moment";

// constants
const NUM_POOL         = "0123456789";
const UPPER_ALPHA_POOL = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
const LOWER_ALPHA_POOL = "abcdefghijklmnopqrstuvwxyz";
const ALPHA_POOL       = UPPER_ALPHA_POOL + LOWER_ALPHA_POOL;
const ALPHA_NUM_POOL   = ALPHA_POOL + NUM_POOL;
const STRING_POOL      = ' !"#$%&\'()*+,-./' + NUM_POOL + ':;<=>?@' + UPPER_ALPHA_POOL + '[\\]^_`' + LOWER_ALPHA_POOL + '{|}~ ';

const POOLS = {
    N: NUM_POOL,
    A: ALPHA_POOL,
    AN: ALPHA_NUM_POOL,
    S: STRING_POOL
}

export interface TokenOptions {    
    length?: number;
    type?: "A" | "N" | "AN" | "S";  
    modifier?: "U" | "L" | "N"; // "U(PPER) - L(OWER) - N(ONE)"
    validityDuration?: number;
    maxNumberOfUse?: number;
    maxNumberOfTries?: number;
    logger?: Logger
}

export interface TokenManagerOptions extends TokenOptions {
    tokenDao: TypeormDao<Token>;
    archTokenDao: TypeormDao<ArchToken>;
}

export default class TokenManager {
    
    private _length: number
    private _type: "A" | "N" | "AN" | "S";
    private _modifier: "U" | "L" | "N";
    private _validityDuration: number
    private _maxNumberOfUse: number
    private _maxNumberOfTries: number

    private _tokenDao: TypeormDao<Token>;
    private _archTokenDao: TypeormDao<ArchToken>;

    private _logger: Logger;    

    constructor(options: TokenManagerOptions) { 
        
        this._length            = options.length            || 8;   
        this._type              = options.type              || 'A';    // Alpha
        this._modifier          = options.modifier          || "N";    // None
        this._validityDuration  = options.validityDuration  || 900000; // 15 min
        this._maxNumberOfUse    = options.maxNumberOfUse    || 1;
        this._maxNumberOfTries  = options.maxNumberOfTries  || 20;

        this._tokenDao = options.tokenDao;
        this._archTokenDao = options.archTokenDao;

        this._logger = options.logger || {} as Logger;
        this._logger;

        this.assertValidMaxNumberOfUse(this._maxNumberOfUse);        
        this.assertValidMaxNumberOfTries(this._maxNumberOfTries);
        this.assertValidityDruation(this._validityDuration);
        this.assertLength(this._length);

    }

    private assertLength(length: number): void {

        if (length < 1) {
            throw new Exception("ERR__TOKEN_SERVICES__TOKEN_MANAGER__INV_LENGTH", {
                message: `The supplied length [${length}  is invalid! It should be a positive integer`,
                maxNumberOfUse: length
            })
        }

    }


    private assertValidMaxNumberOfUse(maxNumberOfUse: number): void {

        if ( maxNumberOfUse < 1 ) {
            throw new Exception("ERR__TOKEN_SERVICES__TOKEN_MANAGER__INV_MAX_NUM_OF_USE", {
                message: `The supplied maxNumberOfUse [${maxNumberOfUse}  is invalid! It should be a positive integer`,
                maxNumberOfUse: maxNumberOfUse
            })
        }

    }

    private assertValidCode(code: string): void {

        Asserts.isNonEmptyString(code, "ERR__TOKEN_SERVICES__TOKEN_MANAGER__INV_CODE", {
            message: `The supplied code [${code} is invalid! It should be a non empty string`,
            code: code
        })

    }


    private assertValidOwner(owner: string): void {

        Asserts.isNonEmptyString(owner, "ERR__TOKEN_SERVICES__TOKEN_MANAGER__INV_OWNER", {
            message: `The supplied owner [${owner} is invalid! It should be a non empty string`,
            code: owner
        })

    }

    private assertValidMaxNumberOfTries(maxNumberOfTries: number): void {

        if (maxNumberOfTries < 1) {
            throw new Exception("ERR__TOKEN_SERVICES__TOKEN_MANAGER__INV_MAX_NUM_OF_TRIES", {
                message: `The supplied maxNumberOfTries [${maxNumberOfTries}  is invalid! It should be a positive integer`,
                maxNumberOfTries: maxNumberOfTries
            })
        }

    }

    private assertValidityDruation(validityDuration: number): void {

        if (validityDuration < 1) {
            throw new Exception("ERR__TOKEN_SERVICES__TOKEN_MANAGER__INV_VALIDITY_DURATION", {
                message: `The supplied validityDuration [${validityDuration}  is invalid! It should be a positive integer`,
                validityDuration: validityDuration
            })
        }

    }

    private async generateSecureRandom(handler: YinzConnection | YinzTransaction, length: number, type: "A" | "N" | "AN" | "S", modifier: "U" | "L" | "N", numberOfTries: number, options?: YinzOptions ): Promise<string> {
        
        numberOfTries = numberOfTries || 0;

        if (numberOfTries > this._maxNumberOfTries) {
            throw new Exception('ERR__TOKEN_SERVICES__TOKEN_MANAGER__TOO_MANY_TRIES', {
                message: 'I have tried ' + numberOfTries + ' times to generate unique secure random but couldn\'t!',
                numberOfTries: numberOfTries,
            });
        }
    
        let random = "";

        crypto.randomBytes(length, (err, buf) => {

            if (err) throw err;

            const pool = POOLS[type];

            random = "";

            for (let index = 0; index < buf.length; index++) {
                random += pool[buf[index] % pool.length];
            }

            // apply modifier
            random = modifier === 'U' ? random.toUpperCase() : modifier === 'L' ? random.toLowerCase() : random;
            

        })

        // TODO extends the options to use isolated
        let tokens = await this._tokenDao.findByFilter(handler, {code: random}, options)

        if (tokens.length > 1) {
            throw new Exception('ERR__TOKEN_SERVICES__TOKEN_MANAGER__TOO_MANY', {
                message: 'There are too many tokens with code ' + random + '!',
                code: random,
            });
        }

        return tokens.length === 0 ? random : this.generateSecureRandom(handler, length, type, modifier, numberOfTries + 1, options );

    }

    public async generateToken(handler: YinzConnection | YinzTransaction, owner: string, data: any, options?: YinzOptions & { token?: TokenOptions}): Promise<Token> {

        let tokenOptions = options && options.token || {} as TokenOptions ;

        tokenOptions.length             = tokenOptions.length            || this._length;
        tokenOptions.type               = tokenOptions.type              || this._type;
        tokenOptions.modifier           = tokenOptions.modifier          || this._modifier;
        tokenOptions.maxNumberOfUse     = tokenOptions.maxNumberOfUse    || this._maxNumberOfUse;
        tokenOptions.validityDuration   = tokenOptions.validityDuration  || this._validityDuration;


        this.assertValidMaxNumberOfUse(tokenOptions.maxNumberOfUse);        
        this.assertValidityDruation(tokenOptions.validityDuration);
        this.assertLength(tokenOptions.length)
        this.assertValidOwner(owner);
        

        let random = await this.generateSecureRandom(handler, tokenOptions.length, tokenOptions.type, tokenOptions.modifier, 0, options);

        let token = new Token();
        token.code = random;
        token.owner = owner;
        token.startValidity = new Date();
        token.validityDuration = tokenOptions.validityDuration;
        token.maxNumberOfUse   = tokenOptions.maxNumberOfUse;
        token.numberOfUse      = 0

        if (data) {
            let dataString = JSON.stringify(data);
            token.data = dataString;
        }
        
        // TODO remember to add isolaated in options
        token = await this._tokenDao.create(handler, token, options)

        token.data = JSON.parse(token.data);

        return token;

    }

    public async lookupToken(handler: YinzConnection | YinzTransaction, code: string, owner: string, options?: YinzOptions): Promise<Token> {

        let refDate = new Date();

        this.assertValidCode(code);        
        if (owner) {
            this.assertValidOwner(owner);
        }

        // TODO remember to add isolated in options
        let tokens = await this._tokenDao.findByFilter(handler, {code: code}, options)

        if ( tokens.length === 0) {
            throw new Exception('ERR__TOKEN_SERVICES__TOKEN_MANAGER__NOT_FOUND', {
                message: 'No token exists with code ' + code + '!',
                code: code,
            });
        }

        if ( tokens.length > 1 ) {
            throw new Exception('ERR__TOKEN_SERVICES__TOKEN_MANAGER__TOO_MANY', {
                message: 'There are too many tokens with code ' + code + '!',
                code: code,
            });
        }

        const token = tokens[0];

        // increase number of use
        token.numberOfUse++;
                                
        // token is expired
        if (isFinite(token.validityDuration) && moment(token.startValidity).add(token.validityDuration, 'ms').isBefore(refDate)) {
            
            let archToken = new ArchToken();
            archToken = { ...token, archOn: refDate, status: "E" };

            // TODO remember to add isolated
            await this._archTokenDao.create(handler, archToken, options);

            // TODO remember to add isolated
            await this._tokenDao.deleteById(handler, token.id , options);

            throw new Exception('ERR__TOKEN_SERVICES__TOKEN_MANAGER__EXPIRED', {
                message: 'The token ' + code + ' has expired!',
                code: code,
                token: token,
            });
        }
        // is the supplied owner is the actual owner of the token
        else if (owner && token.owner !== owner) {
            throw new Exception('ERR__TOKEN_SERVICES__TOKEN_MANAGER__INV_OWNER', {
                message: 'The supplied owner ' + owner + ' doesn\'t own the token ' + code + '!',
                code: code,
                suppliedOwner: owner,
                token: token,
            });
        }

        // is this the last use of the token?
        else if (isFinite(token.maxNumberOfUse) && (token.numberOfUse >= token.maxNumberOfUse)) {
            let archToken = new ArchToken();
            archToken = { ...token, archOn: refDate, status: "B", numberOfUse: token.numberOfUse };

            // TODO remember to add isolated
            await this._archTokenDao.create(handler, archToken, options);

            // TODO remember to add isolated
            await this._tokenDao.deleteById(handler, token.id, options);

            token.data = JSON.parse(token.data);

            return token
        }

        // the token is still usable, update its numberOfUse counter
        else {
            // TODO remember to add isolated
            await this._tokenDao.updateById(handler, { numberOfUse: token.numberOfUse}, token.id, options)

            token.data = JSON.parse(token.data);

            return token;
        }

    }

}