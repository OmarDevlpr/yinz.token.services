// libs
import * as assert from "assert";
import { Chance } from "chance";
import Container from "@yinz/container/ts/Container";
import { TypeormDao, TypeormDataSource } from "@yinz/commons.data";
import { YinzConnection, YinzTransaction } from "@yinz/commons.data/ts/DataSource";
import ArchToken from "@yinz/token.data/models/ArchToken";
import Token from "@yinz/token.data/models/Token";
import TokenManager from "main/ts/TokenManager";
import { Exception } from "@yinz/commons";
import { promisify } from "util";



const container = new Container({
    params: {
        token: {
            length: 8,
            type: "A",
            modifier: "N",
            validityDuration: 900000,
            maxNumberOfUse: 1,
            maxNumberOfTries: 20 
        }
    },
    paramsFile: 'params.yml',
    modules: [
        '@yinz/commons/ts',
        '@yinz/commons.data/ts',
        '@yinz/token.data/ts',
        process.cwd() + '/dist/main/ts'
    ],
});

const chance = new Chance();
// const Exception = container.getClazz('Exception');
const archTokenDao = container.getBean<TypeormDao<ArchToken>>('archTokenDao');
const tokenDao = container.getBean<TypeormDao<Token>>('tokenDao');
const dataSource = container.getBean<TypeormDataSource>('typeormDataSource');

const tokenManager = container.getBean<TokenManager>("tokenManager");

let conn: YinzConnection;
let trans: YinzTransaction;



describe('| access.data.TokenManager <lookupToken>', function () {

    before(() => {
        return new Promise(async (resolve) => {
            conn = await dataSource.getConn();
            resolve(conn);
        })
    });

    after(() => {
        return new Promise(async (resolve) => {
            await dataSource.relConn(conn)
            resolve(true);
        })
    });

    beforeEach(() => {
        return new Promise(async (resolve) => {
            trans = await dataSource.startTrans(conn);
            resolve(trans);
        })
    });

    afterEach(() => {
        return new Promise(async (resolve) => {
            await dataSource.rollbackTrans(trans)
            resolve(true);
        })
    });

    it('\n' +
        '       | Conditions: \n' +
        '       | - maxNumberOfUse is 1 \n' +
        '       | - code is valid : \n' +
        '       | - owner not supplied: \n' +
        '       | - options not supplied: \n' +
        '       | Expected: \n' +
        '       | -> should burn the token with supplied code and return it.' +
        '', async () => {

            // 1. prepare test-data
            let owner = chance.string({ length: 10 });
            let token = new Token();
            let options = { user: "__super_user__" };
            
            token = await tokenManager.generateToken(trans, owner, null, options)

            // 2. execute test        

            let lookupTokenResult =  await tokenManager.lookupToken(trans, token.code, undefined as any, options)

            // 3. check result 
            // 3.1 check generated token

            assert.ok(lookupTokenResult);            
            assert.strictEqual(lookupTokenResult.owner, owner);
            assert.strictEqual(lookupTokenResult.numberOfUse, 1);

            // 3.2 check persisted token

            let findToken = await tokenDao.findByFilter(trans, {owner: owner}, options);

            assert.ok(findToken);
            assert.strictEqual(findToken.length, 0);                        

            // 3.3 check persisted archived token
            let findArchToken = await archTokenDao.findByFilter(trans, { owner: owner }, options);

            assert.ok(findArchToken);
            assert.ok(findArchToken[0].archOn);
            assert.strictEqual(findArchToken.length, 1);                        
            assert.strictEqual(findArchToken[0].owner, owner);                        
            assert.strictEqual(findArchToken[0].numberOfUse, 1);                        
            assert.strictEqual(findArchToken[0].status, "B");                                    

        });


    it('\n' +
        '       | Conditions: \n' +
        '       | - maxNumberOfUse is 2 \n' +
        '       | - code is valid : \n' +
        '       | - owner not supplied: \n' +
        '       | - options not supplied: \n' +
        '       | Expected: \n' +
        '       | -> should return the token and update the numberOfUse.\n' +
        '', async () => {

            // 1. prepare test-data
            let owner = chance.string({ length: 10 });
            let token = new Token();
            let options = { user: "__super_user__" };

            token = await tokenManager.generateToken(trans, owner, null, {...options, token: { maxNumberOfUse: 2}})

            // 2. execute test        

            let lookupTokenResult = await tokenManager.lookupToken(trans, token.code, undefined as any, options)

            // 3. check result 
            // 3.1 check generated token

            assert.ok(lookupTokenResult);
            assert.strictEqual(lookupTokenResult.owner, owner);
            assert.strictEqual(lookupTokenResult.numberOfUse, 1);

            // 3.2 check persisted token

            let findToken = await tokenDao.findByFilter(trans, { owner: owner }, options);

            assert.ok(findToken);
            assert.strictEqual(findToken.length, 1);

            // 3.3 check persisted archived token
            let findArchToken = await archTokenDao.findByFilter(trans, { owner: owner }, options);

            assert.ok(findArchToken);            
            assert.strictEqual(findArchToken.length, 0);            

        });


    it('\n' +
        '       | Conditions: \n' +
        '       | - maxNumberOfUse is 2 \n' +
        '       | - code is valid : \n' +
        '       | - owner not supplied: \n' +
        '       | - options not supplied: \n' +
        '       | - lookup the token twice \n' +
        '       | Expected: \n' +
        '       | -> should burn the token with supplied code and return it. \n' +
        '', async () => {

            // 1. prepare test-data
            let owner = chance.string({ length: 10 });
            let token = new Token();
            let options = { user: "__super_user__" };

            token = await tokenManager.generateToken(trans, owner, null, { ...options, token: { maxNumberOfUse: 2 } })

            // 2. execute test        

            await tokenManager.lookupToken(trans, token.code, undefined as any, options)
            let lookupTokenResult = await tokenManager.lookupToken(trans, token.code, undefined as any, options)

            // 3. check result 
            // 3.1 check generated token

            assert.ok(lookupTokenResult);
            assert.strictEqual(lookupTokenResult.owner, owner);
            assert.strictEqual(lookupTokenResult.numberOfUse, 2);

            // 3.2 check persisted token

            let findToken = await tokenDao.findByFilter(trans, { owner: owner }, options);

            assert.ok(findToken);
            assert.strictEqual(findToken.length, 0);

            // 3.3 check persisted archived token
            let findArchToken = await archTokenDao.findByFilter(trans, { owner: owner }, options);

            assert.ok(findArchToken);
            assert.ok(findArchToken[0].archOn);
            assert.strictEqual(findArchToken.length, 1);
            assert.strictEqual(findArchToken[0].owner, owner);
            assert.strictEqual(findArchToken[0].numberOfUse, 2);
            assert.strictEqual(findArchToken[0].status, "B");                                    


        });


    it('\n' +
        '       | Conditions: \n' +
        '       | - maxNumberOfUse is 1 \n' +
        '       | - code is valid : \n' +
        '       | - owner is supplied: \n' +
        '       | - options not supplied: \n' +        
        '       | Expected: \n' +
        '       | -> should burn the token with supplied code and return it.\n' +
        '', async () => {

            // 1. prepare test-data
            let owner = chance.string({ length: 10 });
            let token = new Token();
            let options = { user: "__super_user__" };

            token = await tokenManager.generateToken(trans, owner, null, { ...options, token: { maxNumberOfUse: 1 } })

            // 2. execute test        
            
            let lookupTokenResult = await tokenManager.lookupToken(trans, token.code, owner, options)

            // 3. check result 
            // 3.1 check generated token

            assert.ok(lookupTokenResult);
            assert.strictEqual(lookupTokenResult.owner, owner);
            assert.strictEqual(lookupTokenResult.numberOfUse, 1);

            // 3.2 check persisted token

            let findToken = await tokenDao.findByFilter(trans, { owner: owner }, options);

            assert.ok(findToken);
            assert.strictEqual(findToken.length, 0);

            // 3.3 check persisted archived token
            let findArchToken = await archTokenDao.findByFilter(trans, { owner: owner }, options);

            assert.ok(findArchToken);
            assert.ok(findArchToken[0].archOn);
            assert.strictEqual(findArchToken.length, 1);
            assert.strictEqual(findArchToken[0].owner, owner);
            assert.strictEqual(findArchToken[0].numberOfUse, 1);
            assert.strictEqual(findArchToken[0].status, "B");


        });


    it('\n' +
        '       | Conditions: \n' +
        '       | - maxNumberOfUse is 2 \n' +
        '       | - code is invalid : \n' +
        '       | - owner is supplied: \n' +
        '       | - options not supplied: \n' +
        '       | Expected: \n' +
        '       | -> should throw error ERR__TOKEN_SERVICES__TOKEN_MANAGER__NOT_FOUND. \n' +
        '', async () => {

            // 1. prepare test-data
            let owner = chance.string({ length: 10 });
            let token = new Token();
            let options = { user: "__super_user__" };

            token = await tokenManager.generateToken(trans, owner, null, { ...options, token: { maxNumberOfUse: 1 } })

            // 2. execute test        

            try {
                await tokenManager.lookupToken(trans, token.code + "z", owner, options)
            } catch (e) {
                // 3. check result
                assert.ok(e instanceof Exception);
                assert.strictEqual(e.reason, "ERR__TOKEN_SERVICES__TOKEN_MANAGER__NOT_FOUND")
            }
                  
        });

    it('\n' +
        '       | Conditions: \n' +
        '       | - maxNumberOfUse is 2 \n' +
        '       | - code is invalid : \n' +
        '       | - owner supplied is invalid \n' +
        '       | - options not supplied: \n' +
        '       | Expected: \n' +
        '       | -> should throw error ERR__TOKEN_SERVICES__TOKEN_MANAGER__INV_OWNER. \n' +
        '', async () => {

            // 1. prepare test-data
            let owner = chance.string({ length: 10 });
            let token = new Token();
            let options = { user: "__super_user__" };

            token = await tokenManager.generateToken(trans, owner, null, { ...options, token: { maxNumberOfUse: 1 } })

            // 2. execute test        

            try {
                await tokenManager.lookupToken(trans, token.code , owner + "z", options)
            } catch (e) {
                // 3. check result
                assert.ok(e instanceof Exception);
                assert.strictEqual(e.reason, "ERR__TOKEN_SERVICES__TOKEN_MANAGER__INV_OWNER")
            }

        });


    it('\n' +
        '       | Conditions: \n' +
        '       | - maxNumberOfUse is 2 \n' +
        '       | - code is invalid : \n' +
        '       | - owner supplied is valid \n' +
        '       | - validityDuration is 1 \n' +
        '       | - options not supplied: \n' +
        '       | Expected: \n' +
        '       | -> should throw error ERR__TOKEN_SERVICES__TOKEN_MANAGER__EXPIRED and archive token \n' +
        '', async () => {

            // 1. prepare test-data
            let owner = chance.string({ length: 10 });
            let token = new Token();
            let options = { user: "__super_user__" };

            token = await tokenManager.generateToken(trans, owner, null, { ...options, token: { validityDuration: 1 } })

            let waitSomeTime = promisify(setTimeout);            
            await waitSomeTime(30);

            // 2. execute test        

            try {
                await tokenManager.lookupToken(trans, token.code, owner , options)

                throw Error("bad flow! we shouldn't be here")
            } catch (e) {
                // 3. check result
                assert.ok(e instanceof Exception);
                assert.strictEqual(e.reason, "ERR__TOKEN_SERVICES__TOKEN_MANAGER__EXPIRED")

                // 3.2
                let findToken = await tokenDao.findByFilter(trans, { owner: owner }, options);

                assert.ok(findToken);
                assert.strictEqual(findToken.length, 0);

                // 3.3 check persisted archived token
                let findArchToken = await archTokenDao.findByFilter(trans, { owner: owner }, options);

                assert.ok(findArchToken);
                assert.ok(findArchToken[0].archOn);
                assert.strictEqual(findArchToken.length, 1);
                assert.strictEqual(findArchToken[0].owner, owner);
                assert.strictEqual(findArchToken[0].numberOfUse, 1);
                assert.strictEqual(findArchToken[0].status, "E");       
            }

        });
    
});

