
import Container from "@yinz/container/ts/Container";
import Logger from "@yinz/commons/ts/Logger";
import { TypeormDao } from "@yinz/commons.data";
import TokenManager from "./TokenManager";
import Token from "@yinz/token.data/models/Token";
import ArchToken from "@yinz/token.data/models/ArchToken";



const registerBeans = (container: Container) => {
    

    let tokenParam = container.getParam("token");

    let tokenManager = new TokenManager({
        tokenDao: container.getBean<TypeormDao<Token>>("tokenDao"),
        archTokenDao: container.getBean<TypeormDao<ArchToken>>("archTokenDao"),
        length : tokenParam && tokenParam["length"],
        type : tokenParam && tokenParam["type"],
        modifier: tokenParam && tokenParam["modifier"],
        validityDuration: tokenParam && tokenParam["validityDuration"],
        maxNumberOfUse: tokenParam && tokenParam["maxNumberOfUse"],
        maxNumberOfTries: tokenParam && tokenParam["maxNumberOfTries"],
        logger: container.getBean<Logger>('logger')
    })
    
    container.setBean('tokenManager', tokenManager);


};

const registerClazzes = (container: Container) => {
    
    container.setClazz('TokenManager', TokenManager);

};


export {
    registerBeans,
    registerClazzes
};